(function () {
  'use strict';

  var Navigation = require('../navigation.page.js');
  var EventDetails = require('./event-details.page.js');
  var EventList = require('./event-list.page.js');
  var EventCreate = require('./event-create.page.js');
  var login = require('../../login.page.js');
  var component = require('../../components.page.js');
  var testhelper = require('../../testhelper.js');

  describe('events', function () {
    var eventCreate, eventList, eventDetails, navigation;

    beforeEach(function () {
      navigation = new Navigation();
      eventCreate = new EventCreate();
      eventDetails = new EventDetails();
      eventList = new EventList();
      login.loginDefaultUser();
    });

    it('create and delete an event', function () {
      var key = Math.floor(Math.random() * 1000000);
      var eventName = 'createTestEvent' + key;
      // create event
      navigation.events.click();
      testhelper.cancelTour();

      eventList.newButton.click();
      eventCreate.page1.name.sendKeys(eventName);
      eventCreate.page1.continueButton.click();
      eventCreate.page2.continueButton.click();
      eventCreate.page3.continueButton.click();
      eventCreate.page4.continueButton.click();

      testhelper.cancelTour();
      expect(eventDetails.title.getText()).toBe(eventName);

      // delete event
      eventDetails.options.settings.click();
      eventDetails.settings.deleteButton.click();
      component.modals.confirm.confirmButton.click();

      // search if event is present
      eventList.search(eventName).then(function (list) {
        expect(list.length).toBe(0);
      });
      eventList.searchInput.clear();
      expect(eventList.search(eventName).count()).toBe(0);

      // clear search field
      eventList.searchInput.clear();
      expect(eventList.searchInput.getText()).toBe('');
    });

  });

})();
