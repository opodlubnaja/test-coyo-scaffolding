(function () {
  'use strict';

  function EventCreate() {
    var api = this;

    var form1 = $('form[name="$ctrl.eventsForm1"]');
    api.page1 = {
      name: element(by.model('$ctrl.event.name')),
      description: element(by.model('$ctrl.event.description')),
      continueButton: form1.$('.btn-primary'),
      cancelButton: form1.$('.btn-default')
    };

    var form2 = $('form[name="$ctrl.eventsForm2"]');
    api.page2 = {
      continueButton: form2.$('.btn-primary'),
      cancelButton: form2.$('.btn-default')
    };

    var form3 = $('form[name="$ctrl.eventsForm3"]');
    api.page3 = {
      continueButton: form3.$('.btn-primary'),
      cancelButton: form3.$('.btn-default')
    };

    var form4 = $('form[name="$ctrl.eventsForm4"]');
    api.page4 = {
      continueButton: form4.$('.btn-primary'),
      cancelButton: form4.$('.btn-default')
    };
  }

  module.exports = EventCreate;

})();
