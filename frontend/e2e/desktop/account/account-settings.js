(function () {
  'use strict';

  var NavigationPage = require('../navigation.page.js');
  var AccountSettingsPage = require('./account-settings.page.js');
  var testhelper = require('../../testhelper.js');
  var login = require('../../login.page.js');
  var components = require('../../components.page.js');

  describe('account settings', function () {
    var navigation, settings, key, firstname, lastname, email, password;

    beforeAll(function () {
      navigation = new NavigationPage();
      settings = new AccountSettingsPage();
      key = Math.floor(Math.random() * 1000000);
      firstname = 'Max';
      lastname = 'Mustermann' + key;
      email = 'max.mustermann.' + key + '@mindsmash.com';
      password = 'Secret123';

      login.loginDefaultUser();
    });

    beforeEach(function () {
      browser.get('/account');
    });

    afterAll(function () {
      // logout current user and login as an admin user
      login.logout();
      login.loginDefaultAdminUser();
      // delete created user
      testhelper.deleteUsers();
    });

    it('should create new user', function () {
      testhelper.createUser(firstname, lastname, email, password, ['User']);
      login.logout();
      login.login(email, password);
      expect(login.isLoggedin()).toBe(true);
      navigation.profileMenu.open();
      navigation.profileMenu.account.click();
      // email will be only changed to check if form works, it won't change anything with the email,
      // because an activation link is send to user when changing email
      expect(settings.displayName.getText()).toBe(firstname + ' ' + lastname);
      expect(settings.email.getText()).toBe(email);
    });

    it('should update account settings', function () {
      var newEmail = 'changed-' + email;
      settings.changeEmail(newEmail);
      expect(settings.email.getText()).toBe(email); // not changed without activation link
      // change password
      settings.password.openModal();
      settings.password.oldPassword.sendKeys(password);
      var newPassword = 'Secret456';
      settings.password.newPassword.sendKeys(newPassword);
      settings.password.confirmNewPassword.sendKeys(newPassword);
      components.modals.confirm.confirmButton.click();
      navigation.logout(); // login.logout() is possible here too, but it's not the normal user process
      login.login(email, newPassword);
      expect(login.isLoggedin()).toBe(true);
    });

    it('update first- and lastname', function () {
      var newFirstname = 'Maximilian';
      var newLastname = 'Musterman';

      expect(settings.displayName.getText()).toBe(firstname + ' ' + lastname);

      settings.changeName(newFirstname, newLastname);
      expect(settings.displayName.getText()).toBe(newFirstname + ' ' + newLastname);
    });

    it('update language', function () {
      settings.language.getText().then(function (currentLanguage) {
        // enter language by typing text
        if (currentLanguage === 'English') {
          var language = 'German';
          settings.changeLanguageByTyping(language);
          expect(settings.language.getText()).toBe('Deutsch');

        } else if (currentLanguage === 'Deutsch') {
          var language1 = 'Englisch';
          settings.changeLanguageByTyping(language1);
          expect(settings.language.getText()).toBe('English');
        }
      });

      // enter language by using selectbox index
      settings.changeLanguageByChooseIndex(1);
      expect(settings.language.getText()).toBe('English');
    });

    it('update timezone', function () {
      settings.changeTimeZone('Adelaide');
      expect(settings.timeZone.getText()).toBe('Adelaide, Darwin');
    });

  });

})();
