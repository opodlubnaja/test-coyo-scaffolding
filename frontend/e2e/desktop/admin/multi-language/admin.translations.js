(function () {
  'use strict';

  var login = require('./../../../login.page.js');
  var translations = require('./admin.translations.page');

  describe('translations administration', function () {

    beforeEach(function () {
      login.loginDefaultUser();
      translations.get();
    });

    it('should show translations form', function () {

      expect(translations.form.search.isDisplayed()).toBe(true);
      expect(translations.form.languageFilter.isPresent()).toBe(true);
      expect(translations.form.adaptedTranslationsCheckbox.isDisplayed()).toBe(true);
      expect(translations.form.table.isDisplayed()).toBe(true);
    });
  });
})();
