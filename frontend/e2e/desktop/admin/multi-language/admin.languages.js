(function () {
  'use strict';

  var login = require('./../../../login.page.js');
  var languages = require('./admin.languages.page');

  describe('languages administration', function () {

    beforeEach(function () {
      login.loginDefaultUser();
      languages.get();
    });

    it('should show languages form', function () {

      expect(languages.form.table.isDisplayed()).toBe(true);
    });
  });
})();
