(function () {
  'use strict';

  var Select = require('../../../select.page.js');
  var Checkbox = require('../../../checkbox.page');

  module.exports = {
    getById: function (id) {
      browser.get('/admin/user-directories/edit/' + id);
    },
    name: element(by.model('$ctrl.userDirectory.name')),
    type: new Select(element(by.model('$ctrl.userDirectory.type')), false),
    active: new Checkbox(element(by.model('$ctrl.userDirectory.active'))),
    saveButton: element(by.cssContainingText('.btn.btn-primary', 'Save')),
    isSaveButtonDisabled: function () {
      return this.saveButton.getAttribute('disabled').then(function (disabled) {
        return disabled === 'true';
      });
    },
    cancelButton: element(by.cssContainingText('.btn.btn-default', 'Cancel'))
  };

})();
