(function () {
  'use strict';
  /* global require, Promise */

  var pageIds = [];
  var userIds = [];
  var timelineItemsIds = [];
  var timelineIds = [];
  var workspaceIds = [];

  function createUser(firstname, lastname, email, password, roles) {
    return new Promise(function (resolve) {
      resolve(browser.executeAsyncScript(function (firstname, lastname, email, password, roles, callback) {
        function loadUser(roleIds) {
          return new UserModel({
            active: true,
            superadmin: false,
            firstname: firstname,
            lastname: lastname,
            email: email,
            password: password,
            roleIds: roleIds,
            groupIds: []
          }).save().then(function (user) {
            callback({userId: user.id});
          }).catch(function (error) {
            callback(error);

          });
        }

        var injector = $('body').injector();
        var RoleModel = injector.get('RoleModel');
        var UserModel = injector.get('UserModel');
        if (roles && roles.length > 0) {
          return RoleModel.query().then(function (result) {
            var roleIds = _.filter(result.content, function (role) {
              return roles.indexOf(role.displayName) >= 0;
            }).map(function (role) {
              return role.id;
            });
            return loadUser(roleIds);
          }).catch(function (error) {
            callback(error);
          });
        } else {
          return loadUser([]);
        }
      }, firstname, lastname, email, password, roles).then(function (result) {
        if (result.error) {
          fail('Creating user account failed: ' + JSON.stringify(result.error));
        }
        userIds.push(result.userId);
      }));
    });
  }


  /**
   * Delete all users created by createUser via direct AJAX call.
   */
  function deleteUsers() {
    userIds.forEach(function (userId) {
      browser.executeAsyncScript(function (userId, callback) {
        var injector = $('body').injector();
        var UserModel = injector.get('UserModel');
        new UserModel({id: userId}).delete().then(function () {
          callback();
        }).catch(function (error) {
          callback(error);
        });
      }, userId).then(function (error) {
        if (error) {
          console.log('Deleting user failed', JSON.stringify(error)); //eslint-disable-line
        }
      });
    });
    userIds = [];
  }

  /**
   * Helper function to create and open a basic page without having to use the UI.
   * This is intended to be used in tests that need a clean page environment to work in
   * but don't care about the page or how it is created (esp. app + widget tests).
   *
   * @param {string} name The name of the page
   * @returns {object} promise to be resolved when the script is finished
   */
  function createAndOpenPage(name) {
    return createPage(name).then(function (pageId) {
      browser.get('/pages/' + pageId);
      disableAnimations();
      cancelTour();
      return pageId;
    });
  }

  /**
   * Helper function to create a basic page without having to use the UI.
   * This is intended to be used in tests that need a clean page environment to work in
   * but don't care about the page or how it is created (esp. app + widget tests).
   *
   * @param {string} name The name of the page
   * @param {string} visibility for public or private
   * @returns {object} promise to be resolved when the script is finished
   */
  function createPage(name, visibility) {
    return createPageWithSingleCategory(name, '', '', '', visibility);
  }

  /**
   * Helper function to get the category id by category name.
   *
   * @param {string} categoryName The name of a category
   * @param {string} model The category model name
   */
  function getCategoryId(categoryName, model) {
    var categoryId;
    return browser.executeAsyncScript(function (categoryName, model, callback) {
      var injector = $('body').injector();
      var CategoryModel = injector.get(model);
      CategoryModel.get('all', {name: categoryName}).then(function (category) {
        categoryId = category[0].id;
        callback({categoryId: categoryId});
      }).catch(function (error) {
        callback({error: error});
      });
    }, categoryName, model).then(function (result) {
      if (result.error) {
        fail('Creating category "' + categoryName + '" failed: ' + JSON.stringify(result.error));
      }
      return result.categoryId;
    });
  }

  /**
   * Helper function to create a basic page with a category without having to use the UI.
   * This is intended to be used in tests that need a clean page environment to work in
   * but don't care about the page or how it is created (esp. app + widget tests).
   *
   * @param {string} name The name of the page
   * @param {string} description The description of the page
   * @param {string} categoryId category id
   * @param {string} categoryName category name
   * @param {string} visibility for public or private
   * @returns {object} promise to be resolved when the script is finished
   */
  function createPageWithSingleCategory(name, description, categoryId, categoryName, visibility) {
    if (!visibility) {
      visibility = 'PUBLIC';
    }
    return browser.executeAsyncScript(function (name, description, categoryId, categoryName, visibility, callback) {
      var injector = $('body').injector();
      var PageModel = injector.get('PageModel');
      var authService = injector.get('authService');
      return authService.getUser().then(function (user) {
        return new PageModel({
          name: name,
          description: description,
          visibility: visibility,
          adminIds: [user.id],
          adminGroupIds: [],
          memberGroupIds: [],
          memberIds: [],
          categories: [{
            id: categoryId,
            displayName: categoryName
          }],
          categoryIds: [categoryId],
          autoSubscribe: false,
          autoSubscribeType: 'NONE',
          autoSubscribeUserIds: [],
          autoSubscribeGroupIds: []
        }).save().then(function (page) {
          callback({pageId: page.id});
        }).catch(function (error) {
          callback({error: error});
        });
      }).catch(function (error) {
        callback({error: error});
      });
    }, name, description, categoryId, categoryName, visibility).then(function (result) {
      if (result.error) {
        fail('Creating page failed: ' + JSON.stringify(result.error));
      }
      pageIds.push(result.pageId);
      return result.pageId;
    });
  }

  /**
   * Helper function to create a basic page with multiple categories without having to use the UI.
   * This is intended to be used in tests that need a clean page environment to work in
   * but don't care about the page or how it is created (esp. app + widget tests).
   *
   * @param {string} name The name of the page
   * @param {string} description The description of the page
   * @param {object} categories the categories object
   * @param {string} visibility for public or private
   * @returns {object} promise to be resolved when the script is finished
   */
  function createPageWithMultipleCategories(name, description, categories, visibility) {
    if (!visibility) {
      visibility = 'PUBLIC';
    }
    return browser.executeAsyncScript(function (name, description, categories, visibility, callback) {
      var injector = $('body').injector();
      var PageModel = injector.get('PageModel');
      var authService = injector.get('authService');
      return authService.getUser().then(function (user) {
        return new PageModel({
          name: name,
          description: description,
          visibility: visibility,
          adminIds: [user.id],
          adminGroupIds: [],
          memberGroupIds: [],
          memberIds: [],
          categories: categories,
          categoryIds: _.map(categories, 'categoryId'),
          autoSubscribe: false,
          autoSubscribeType: 'NONE',
          autoSubscribeUserIds: [],
          autoSubscribeGroupIds: []
        }).save().then(function (page) {
          callback({pageId: page.id});
        }).catch(function (error) {
          callback({error: error});
        });
      }).catch(function (error) {
        callback({error: error});
      });
    }, name, description, categories, visibility).then(function (result) {
      if (result.error) {
        fail('Creating page failed: ' + JSON.stringify(result.error));
      }
      pageIds.push(result.pageId);
      return result.pageId;
    });
  }


  /**
   * Delete all pages created by createAndOpenPage via direct AJAX call.
   */
  function deletePages() {
    pageIds.forEach(function (pageId) {
      browser.executeAsyncScript(function (pageId, callback) {
        var injector = $('body').injector();
        var PageModel = injector.get('PageModel');
        new PageModel({id: pageId}).delete().then(function () {
          callback();
        }).catch(function (error) {
          callback(error);
        });
      }, pageId).then(function (error) {
        if (error) {
          console.log('Deleting page failed', JSON.stringify(error)); //eslint-disable-line
        }
      });
    });
    pageIds = [];
  }

  function hasTestPages() {
    return pageIds.length > 0;
  }

  /**
   * Helper function to create a timeline for a sender
   *
   * @param {string} senderId The id of a sender (e.g. page)
   * @param {string} timelineName The name of the timeline
   * @returns {string} promise timeline id
   */
  function createTimeline(senderId, timelineName) {
    return browser.executeAsyncScript(function (senderId, timelineName, callback) {
      var injector = $('body').injector();
      var AppModel = injector.get('AppModel');
      return new AppModel({
        senderId: senderId,
        key: 'timeline',
        name: timelineName,
        active: true,
        settings: {}
      }).save().then(function (timeline) {
        callback({timelineId: timeline.id});
      }).catch(function (error) {
        callback({error: error});
      });
    }, senderId, timelineName).then(function (result) {
      if (result.error) {
        fail('Creating timeline failed: ' + JSON.stringify(result.error));
      }
      var timelineIdList = [];
      timelineIdList.push(result.timelineId);
      timelineIdList.push(senderId);
      timelineIds.push(timelineIdList);
      return result.timelineId;
    });
  }

  /**
   * Helper function to delete all timelines created by createTimeline
   */
  function deleteTimelines() {
    timelineIds.forEach(function (timelineIdList) {
      var timelineId = timelineIdList[0];
      var senderId = timelineIdList[1];
      browser.executeAsyncScript(function (timelineId, senderId, callback) {
        var injector = $('body').injector();
        var AppModel = injector.get('AppModel');
        var url = AppModel.$url({senderId: senderId, id: timelineId});
        new AppModel({
          id: timelineId,
          senderId: senderId
        }).$delete(url, {deleteAppFiles: true}).then(function () {
          callback();
        }).catch(function (error) {
          callback(error);
        });
      }, timelineId, senderId).then(function (error) {
        if (error) {
          console.log('Deleting timeline failed', JSON.stringify(error)); //eslint-disable-line
        }
      });
    });
    timelineIds = [];
  }

  /**
   * Helper function to create a timeline item
   *
   * @param {string} message message to be posted on timeline
   * @param {string} senderId recipient id for message
   * @returns {string} promise timelineitem id
   */
  function createTimelineEntry(message, senderId, timelineItemOverrides) {
    return browser.executeAsyncScript(function (message, senderId, timelineItemOverrides, callback) {
      var injector = $('body').injector();
      var TimelineItemModel = injector.get('TimelineItemModel');
      var authService = injector.get('authService');
      return authService.getUser().then(function (user) {
        var timelineItem = {
          authorId: user.id,
          data: {
            message: message
          },
          type: 'post',
          recipientIds: [senderId]
        };
        angular.extend(timelineItem, timelineItemOverrides);
        return new TimelineItemModel(timelineItem).save().then(function (timelineitem) {
          callback({timelineitemId: timelineitem.id});
        }).catch(function (error) {
          callback({error: error});
        });
      }).catch(function (error) {
        callback({error: error});
      });
    }, message, senderId, timelineItemOverrides).then(function (result) {
      if (result.error) {
        fail('Creating timeline item failed: ' + JSON.stringify(result.error));
      }
      timelineItemsIds.push(result.timelineitemId);
      return result.timelineitemId;
    });
  }

  /**
   * Helper function to delete all timelineitems created by createTimelineEntry
   */
  function deleteTimelineEntry() {
    timelineItemsIds.forEach(function (timelineId) {
      browser.executeAsyncScript(function (timelineId, callback) {
        var injector = $('body').injector();
        var TimelineItemModel = injector.get('TimelineItemModel');
        new TimelineItemModel({id: timelineId}).delete().then(function () {
          callback();
        }).catch(function (error) {
          callback(error);
        });
      }, timelineId).then(function (error) {
        if (error) {
          console.log('Deleting timelineItem failed', JSON.stringify(error)); //eslint-disable-line
        }
      });
    });
    timelineItemsIds = [];
  }

  /**
   * creates a workspace without using UI
   *
   * @param {string} name name of the workspace
   * @param {string} visibility either PUBLIC, PRIVATE or CLOSED
   * @returns {Promise<R>} workspace id
   */
  function createWorkspace(name, visibility) {
    if (!visibility) {
      visibility = 'PUBLIC';
    }
    return browser.executeScript(function (name, visibility, callback) {
      var injector = $('body').injector();
      var WorkspaceModel = injector.get('WorkspaceModel');
      var authService = injector.get('authService');
      return authService.getUser().then(function (user) {
        return new WorkspaceModel({
          name: name,
          visibility: visibility,
          adminIds: [user.id],
          adminGroupIds: [],
          categories: null,
          categoryIds: [],
          memberGroupIds: [],
          memberIds: []
        }).save().then(function (workspace) {
          callback({workspaceId: workspace.id});
        }).catch(function (error) {
          callback({error: error});
        });
      }).catch(function (error) {
        callback({error: error});
      });
    }, name, visibility).then(function (result) {
      if (result.error) {
        fail('Creating workspace failed: ' + JSON.stringify(result.error));
      }
      workspaceIds.push(result.workspaceId);
      return result.workspaceId;
    });
  }

  /**
   * Creates a workspace with multiple categories without using UI.
   *
   * @param {string} name The name of the workspace
   * @param {string} description The description of the workspace
   * @param {object} categories The categories object
   * @param {string} visibility Either PUBLIC, PRIVATE or CLOSED
   * @returns {object} promise to be resolved when the script is finished
   */
  function createWorkspaceWithMultipleCategories(name, description, categories, visibility) {
    if (!visibility) {
      visibility = 'PUBLIC';
    }
    return browser.executeAsyncScript(function (name, description, categories, visibility, callback) {
      var injector = $('body').injector();
      var WorkspaceModel = injector.get('WorkspaceModel');
      var authService = injector.get('authService');
      return authService.getUser().then(function (user) {
        return new WorkspaceModel({
          name: name,
          description: description,
          visibility: visibility,
          adminIds: [user.id],
          adminGroupIds: [],
          categories: categories,
          categoryIds: _.map(categories, 'categoryId'),
          memberGroupIds: [],
          memberIds: []
        }).save().then(function (workspace) {
          callback({workspaceId: workspace.id});
        }).catch(function (error) {
          callback({error: error});
        });
      }).catch(function (error) {
        callback({error: error});
      });
    }, name, description, categories, visibility).then(function (result) {
      if (result.error) {
        fail('Creating workspace failed: ' + JSON.stringify(result.error));
      }
      workspaceIds.push(result.workspaceId);
      return result.workspaceId;
    });
  }

  /**
   * Deletes all workspaces created with createWorkspace(name, visibility)
   *
   */
  function deleteWorkspace() {
    workspaceIds.forEach(function (workspaceId) {
      browser.executeAsyncScript(function (workspaceId, callback) {
        var injector = $('body').injector();
        var WorkspaceModel = injector.get('WorkspaceModel');
        new WorkspaceModel({id: workspaceId}).delete().then(function () {
          callback();
        }).catch(function (error) {
          callback(error);
        });
      }, workspaceId).then(function (error) {
        if (error) {
          console.log('Deleting workspace failed', JSON.stringify(error)); //eslint-disable-line
        }
      });
    });
    workspaceIds = [];
  }

  /**
   * Sets singleRole to the global default role
   *
   * @param {string} singleRole name of a role like 'User' or 'Admin'
   */
  function setDefaultRole(singleRole) {
    browser.executeAsyncScript(function (singleRole, callback) {
      function changePermissions(id) {
        // get role with id
        return RoleModel.get({id: id}).then(function (role) {
          var groupIds = role.groups.map(function (elem) {
            return elem.id;
          });
          role.defaultRole = true;
          role.groupIds = groupIds;
          // update/put role with id
          role.save().then(function (role) {
            callback({roleId: role.id});
          }).catch(function (error) {
            callback(error);
          });
        }).catch(function (error) {
          callback(error);
        });
      }

      var injector = $('body').injector();
      var RoleModel = injector.get('RoleModel');
      // get all roles
      return RoleModel.query().then(function (result) {
        var ids = _.filter(result.content, function (role) {
          return singleRole === role.displayName;
        }).map(function (role) {
          return role.id;
        });
        return changePermissions(ids[0]);
      }).catch(function (error) {
        callback(error);
      });
    }, singleRole).then(function (result) {
      if (result.error) {
        fail('set default role failed: ' + JSON.stringify(result.error));
      }
    });
  }

  /*
  * Prevent the browser window from closing when the test failed to close the widget edit mode and an alert pops up
   */
  function ignorePageLeaveAlert() {
    browser.executeScript('window.onbeforeunload = function(){};');
  }

  /**
   * protractor does not know about css3 animations as they are not part of the angular digest cycle and thus
   * aitForAngular does wait for animations to finish. As a workaround we simply disable all transitions for testing.
   */
  function disableAnimations() {
    var css = '* {' +
        '-webkit-transition: none !important;' +
        '-moz-transition: none !important;' +
        '-o-transition: none !important;' +
        '-ms-transition: none !important;' +
        'transition: none !important;' +
        'animation: none !important;' +
        '-webkit-animation: none !important' +
        '-moz-animation: none !important;' +
        '-o-animation: none !important;' +
        '-ms-animation: none !important;' +
        '}';
    var script = '$(\'<style type=\"text/css\">' + css + '</style>\').appendTo($(\'body\'));';  // eslint-disable-line no-useless-escape
    browser.executeScript(script);
    browser.sleep(100);
  }

  /**
   * Cancel any tour popup if it is present, ignore otherwise
   */
  function cancelTour() {
    var tourClose = $('.tour-step-close');
    tourClose.isPresent().then(function (isPresent) {
      if (isPresent) {
        tourClose.click();
      }
    });
  }

  function hasClass(element, cls) {
    return element.getAttribute('class').then(function (classes) {
      return classes.split(' ').indexOf(cls) !== -1;
    });
  }

  module.exports = {
    createUser: createUser,
    deleteUsers: deleteUsers,
    createAndOpenPage: createAndOpenPage,
    createPage: createPage,
    createPageWithSingleCategory: createPageWithSingleCategory,
    createPageWithMultipleCategories: createPageWithMultipleCategories,
    deletePages: deletePages,
    hasTestPages: hasTestPages,
    createTimeline: createTimeline,
    deleteTimelines: deleteTimelines,
    createTimelineEntry: createTimelineEntry,
    deleteTimelineEntry: deleteTimelineEntry,
    createWorkspace: createWorkspace,
    createWorkspaceWithMultipleCategories: createWorkspaceWithMultipleCategories,
    deleteWorkspace: deleteWorkspace,
    disableAnimations: disableAnimations,
    setDefaultRole: setDefaultRole,
    cancelTour: cancelTour,
    getCategoryId: getCategoryId,
    hasClass: hasClass,
    ignorePageLeaveAlert: ignorePageLeaveAlert
  };

})();
