(function () {
  'use strict';

  var testhelper = require('../testhelper');

  function MobileNavigation() {
    var api = this;

    var backdrop = $('.backdrop');

    api.naviagtion = {
      open: function () {
        $('a[ng-click="$ctrl.openMenu()"]').click();
      },
      close: function () {
        var xCoordinate = 350;
        browser.manage().window().getSize().then(function (size) {
          xCoordinate = size.width - (size.width / 4);
          browser.actions()
              .mouseMove(backdrop, {x: xCoordinate, y: 5})
              .click()
              .perform();
        });
      },
      navbar: $('.sidebar.slide-in'),

      group: {
        open: function (name) {
          var elem = $('ul[ng-class="{\'nav-collapsed-false\': !$ctrl.navState[\'' + name + '\'] }"]');
          return testhelper.hasClass(elem, 'nav-collapsed-false').then(function (boolVal) {
            if (boolVal) {
              $('li[ng-click="$ctrl.navCollapse(\'' + name + '\')"]').click();
            }
          });
        }
      },

      groupHome: {
        open: function () {
          return api.naviagtion.group.open('homepages');
        }
      },

      groupMain: {
        open: function () {
          return api.naviagtion.group.open('main');
        },
        profile: $('a[ui-sref="main.profile.current"]')
      },

      groupMyPages: {
        open: function () {
          return api.naviagtion.group.open('pages');
        }
      },

      groupMyWorkspaces: {
        open: function () {
          return api.naviagtion.group.open('workspaces');
        }
      },

      groupMore: {
        elem: $('ul[ng-class="{\'nav-collapsed-false\': !$ctrl.navState[\'more\'] }"]'),
        open: function () {
          return api.naviagtion.group.open('more');
        },
        logoutButton: $('a[ng-click="$ctrl.logout()"]')
      }
    };

    api.notifications = {
      open: function () {
        $('.notifications-dialog-parent').$('a[ng-click="$ctrl.toggle();$event.stopPropagation()"]').click();
      },
      close: function () {
        api.notifications.open();
      },
      notificationsDialog: $('.notifications-dialog')
    };

    api.messaging = {
      open: function () {
        $('a[ng-click="$ctrl.openMessagingSidebar()"]').click();
      },
      close: function () {
        browser.actions()
            .mouseMove(backdrop, {x: 5, y: 5})
            .click()
            .perform();
      },
      messagingbar: $('.messaging.sidebar.slide-in')
    };

    api.logout = function () {
      api.naviagtion.open();
      api.naviagtion.groupMore.open();
      api.naviagtion.groupMore.logoutButton.click();
    };
  }

  module.exports = MobileNavigation;

})();
